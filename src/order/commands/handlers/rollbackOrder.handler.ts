import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { OrderRepository } from 'src/repository/order.repository';
import { RollbackOrderCommand } from '../impl/rollbackOrder.command';

@CommandHandler(RollbackOrderCommand)
export class RollbackOrderHandler
  implements ICommandHandler<RollbackOrderCommand>
{
  constructor(private repository: OrderRepository) {}

  async execute(command: RollbackOrderCommand) {
    try {
      await this.repository.rollbackOrder(command.orderId);
    } catch (error) {
      throw error;
    }
  }
}
