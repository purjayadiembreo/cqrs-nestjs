import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { OrderRepository } from 'src/repository/order.repository';
import { CreateOrderCommand } from '../impl/createOder.command';
import { OrderCreatedEvent } from '../../events/impl/orderCreated.command';

@CommandHandler(CreateOrderCommand)
export class CreateOrderHandler implements ICommandHandler<CreateOrderCommand> {
  constructor(
    private repository: OrderRepository,
    private readonly eventBus: EventBus,
  ) {}

  /**
   * This function creates an order and publishes an event if successful, otherwise it throws an error.
   * @param {CreateOrderCommand} command - The parameter `command` is an object of type
   * `CreateOrderCommand` that contains information necessary to create a new order. It likely includes
   * properties such as the customer's information, the items being ordered, and any special
   * instructions or notes. The `execute` function is responsible for taking this command and
   */
  async execute(command: CreateOrderCommand) {
    try {
      await this.repository.create(command.order);
      this.eventBus.publish(new OrderCreatedEvent(command.order));
    } catch (error) {
      throw error;
    }
  }
}
