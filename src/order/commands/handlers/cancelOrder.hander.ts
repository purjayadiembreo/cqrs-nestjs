import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { OrderRepository } from 'src/repository/order.repository';
import { CancelOrderCommand } from '../impl/cancelOrder.command';
import { OrderCanceledEvent } from 'src/order/events/impl/orderCancelled.command';

@CommandHandler(CancelOrderCommand)
export class CancelOrderHandler implements ICommandHandler<CancelOrderCommand> {
  constructor(
    private repository: OrderRepository,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: CancelOrderCommand) {
    try {
      await this.repository.cancelOrder(command.orderId);
      this.eventBus.publish(new OrderCanceledEvent(command.orderId));
    } catch (error) {
      throw error;
    }
  }
}
