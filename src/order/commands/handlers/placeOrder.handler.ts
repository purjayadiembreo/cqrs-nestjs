import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { OrderRepository } from 'src/repository/order.repository';
import { PlaceOrderCommand } from '../impl/placeOrder.command';
import { HttpStatus } from '@nestjs/common';

@CommandHandler(PlaceOrderCommand)
export class PlaceOrderHandler implements ICommandHandler<PlaceOrderCommand> {
  constructor(private repository: OrderRepository) {}

  async execute(command: PlaceOrderCommand) {
    try {
      await this.repository.processOrder(command.order);
      return {
        success: true,
        statusCode: HttpStatus.CREATED,
        message: 'Order success',
      };
    } catch (error) {
      throw error;
    }
  }
}
