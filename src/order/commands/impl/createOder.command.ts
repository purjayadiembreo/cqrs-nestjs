import { OrderDTO } from '../../dto/order.dto';

export class CreateOrderCommand {
  constructor(public readonly order: OrderDTO) {}
}
