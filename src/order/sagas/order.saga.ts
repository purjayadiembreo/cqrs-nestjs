import { Injectable } from '@nestjs/common';
import { Saga, ofType } from '@nestjs/cqrs';
import { Observable, from, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { OrderCreatedEvent } from '../events/impl/orderCreated.command';
import { PlaceOrderCommand } from '../commands/impl/placeOrder.command';
import { CancelOrderCommand } from '../commands/impl/cancelOrder.command';
import { PaymentRepository } from 'src/repository/payment.repository';
import { InventoryRepository } from 'src/repository/inventory.repository';
import { OrderCanceledEvent } from '../events/impl/orderCancelled.command';
import { RollbackOrderCommand } from '../commands/impl/rollbackOrder.command';

@Injectable()
export class OrderSaga {
  constructor(
    private readonly inventory: InventoryRepository,
    private readonly payment: PaymentRepository,
  ) {}

  @Saga()
  orderSaga = (events$: Observable<any>): Observable<any> => {
    return events$.pipe(
      /* triggered when an `OrderCreatedEvent` is emitted. */
      ofType(OrderCreatedEvent),
      switchMap((event: OrderCreatedEvent) => {
        return from(this.inventory.checkInventory(event.order.productId)).pipe(
          map(() => new PlaceOrderCommand(event.order)),
          catchError(() => of(new CancelOrderCommand(event.order.id))),
        );
      }),
      /* triggered when an OrderCanceledEvent is emitted. */
      ofType(OrderCanceledEvent),
      switchMap((event: OrderCanceledEvent) => {
        return from(this.payment.refundPayment(event.orderId)).pipe(
          map(() => new RollbackOrderCommand(event.orderId)),
        );
      }),
    );
  };
}
