export class OrderDTO {
  readonly id: string;
  readonly productId: string;
  readonly quantity: number;
  readonly date: string;
}
