import { Module } from '@nestjs/common';

import { CqrsModule } from '@nestjs/cqrs';
import { OrderController } from './order.controller';
import { OrderSaga } from './sagas/order.saga';
import { PaymentRepository } from '../repository/payment.repository';
import { InventoryRepository } from '../repository/inventory.repository';
import { OrderRepository } from '../repository/order.repository';
import { CreateOrderHandler } from './commands/handlers/createOrder.hander';
import { PlaceOrderHandler } from './commands/handlers/placeOrder.handler';

@Module({
  imports: [CqrsModule],
  controllers: [OrderController],
  providers: [
    OrderSaga,
    PaymentRepository,
    InventoryRepository,
    OrderRepository,
    CreateOrderHandler,
    PlaceOrderHandler,
  ],
})
export class OrderModule {}
