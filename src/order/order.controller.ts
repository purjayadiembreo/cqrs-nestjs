import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { OrderDTO } from './dto/order.dto';
import { CreateOrderCommand } from './commands/impl/createOder.command';
import { GetOrderQuery } from './queries/impl/getOrder.query';

@Controller('order')
export class OrderController {
  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {}

  @Post()
  async create(@Body() payload: OrderDTO) {
    return await this.commandBus.execute(new CreateOrderCommand(payload));
  }

  @Get()
  getListOrder(@Query() query: any) {
    return this.queryBus.execute(new GetOrderQuery(query));
  }
}
