import { OrderDTO } from '../../dto/order.dto';

export class OrderCreatedEvent {
  constructor(public readonly order: OrderDTO) {}
}
