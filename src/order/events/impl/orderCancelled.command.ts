export class OrderCanceledEvent {
  constructor(public readonly orderId: string) {}
}
