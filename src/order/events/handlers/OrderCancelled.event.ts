import { Logger } from '@nestjs/common';
import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { OrderCanceledEvent } from '../impl/orderCancelled.command';

@EventsHandler(OrderCanceledEvent)
export class OrderCanceledHandler implements IEventHandler<OrderCanceledEvent> {
  async handle(event: OrderCanceledEvent) {
    Logger.log('Canceled order...', event);
  }
}
