export class GetOrderQuery {
  constructor(
    public readonly orderId?: number,
    public readonly limit?: number,
    public readonly offset?: number,
  ) {}
}
