import { HttpStatus } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetOrderQuery } from '../impl/getOrder.query';
import { OrderRepository } from 'src/repository/order.repository';

@QueryHandler(GetOrderQuery)
export class GetOrderHandler implements IQueryHandler<GetOrderQuery> {
  constructor(private repository: OrderRepository) {}
  async execute(query: GetOrderQuery) {
    try {
      const result = await this.repository.findMany(query);
      return {
        success: true,
        statusCode: HttpStatus.OK,
        message: 'Success get order',
        result,
      };
    } catch (error) {
      throw error;
    }
  }
}
