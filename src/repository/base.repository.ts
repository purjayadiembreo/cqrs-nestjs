import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export abstract class BaseRepository<T> extends PrismaService {
  protected readonly prisma: PrismaService;
  constructor(private readonly modelName: string) {
    super();
    this.prisma = new PrismaService();
  }

  async findMany(options: any): Promise<T[]> {
    const { take, skip, includes, where, search } = options;
    const query = { take, skip, include: {}, where: {} };
    if (includes) {
      includes.map((inc: any) => {
        query.include[inc] = true;
      });
    } else {
      delete query.include;
    }
    if (Array.isArray(where) && search) {
      const whereQuery = where.map((field) => {
        return {
          [field]: {
            contains: search,
          },
        };
      });
      query.where = {
        OR: whereQuery,
      };
    }
    query.skip = query.take ? (query.skip - 1) * query.take : undefined;
    return this.prisma[this.modelName].findMany(query);
  }

  async findById(id: number): Promise<T> {
    return this.prisma[this.modelName].findUnique({ where: { id } });
  }

  async create(data: any): Promise<T> {
    return this.prisma[this.modelName].create({ data });
  }

  async update(id: string, data: any): Promise<T> {
    return this.prisma[this.modelName].update({ where: { id }, data });
  }

  async delete(id: string): Promise<void> {
    return this.prisma[this.modelName].delete({ where: { id } });
  }

  async count(): Promise<void> {
    return this.prisma[this.modelName].count();
  }
}
