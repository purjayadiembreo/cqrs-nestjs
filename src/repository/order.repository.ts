import { Injectable } from '@nestjs/common';
import { BaseRepository } from './base.repository';

@Injectable()
export class OrderRepository extends BaseRepository<'Order'> {
  constructor() {
    super('order');
  }
}
