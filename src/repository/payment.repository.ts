import { Injectable } from '@nestjs/common';
import { BaseRepository } from './base.repository';

@Injectable()
export class PaymentRepository extends BaseRepository<'Payment'> {
  constructor() {
    super('payment');
  }

  async refundPayment(orderId: string) {
    try {
      return orderId;
    } catch (error) {
      throw error;
    }
  }
}
