import { Injectable } from '@nestjs/common';
import { BaseRepository } from './base.repository';

@Injectable()
export class InventoryRepository extends BaseRepository<'Inventory'> {
  constructor() {
    super('Inventory');
  }

  async checkInventory(id: string) {
    try {
      return id;
    } catch (error) {
      return false;
    }
  }

  async decreaseStock() {
    try {
      return true;
    } catch (error) {
      return false;
    }
  }
}
